import React, { Component } from "react";
import "./App.css";
import { Route } from "react-router-dom";

import SignUpPage from "./components/pages/signUpPage";
import LoginPage from "./components/pages/loginPage";
import ForgotPassword from "./components/pages/forgotPassword";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route exact path="/" component={SignUpPage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/forgot-password" component={ForgotPassword} />
      </div>
    );
  }
}

export default App;
