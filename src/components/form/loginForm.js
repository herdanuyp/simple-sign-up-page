import React from "react";
import { Form } from "semantic-ui-react";
import { Link } from "react-router-dom";

import FormErrors from "../errorMessage";

class SignUpForm extends React.Component {
  state = {
    email: "",
    password: "",
    formErrors: {
      email: "",
      password: ""
    },
    emailValid: false,
    passwordValid: false,
    loadingForm: false
  };

  handleChange = (e, { name, value }) => {
    this.setState(
      {
        [name]: value
      },
      () => {
        this.validateField(name, value);
      }
    );
  };

  validateField = (fieldName, value) => {
    let fieldValidationErrors = this.state.formErrors;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;

    switch (fieldName) {
      case "email":
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailValid ? "" : "Email is invalid";
        break;
      case "password":
        passwordValid = value.length >= 8;
        fieldValidationErrors.password = passwordValid
          ? ""
          : "Password is too short";
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      emailValid: emailValid,
      passwordValid: passwordValid
    });
  };

  submit = async e => {
    e.preventDefault();
    if (!this.state.email) {
      alert("Email is required");
    } else if (!this.state.password) {
      alert("Password is required");
    } else {
      localStorage.setItem("email", this.state.email);
      localStorage.setItem("password", this.state.password);
      
      await this.setState({
        password: "",
        email: "",
        loadingForm: true
      });
 
      setTimeout(async () => {
        await this.setState({
          loadingForm: false
        });

        alert(`You are logged in`);
      }, 1500);
    }
  };

  render() {
    const { email, password } = this.state;
    return (
      <div className="form-content-login">
        <Form size="large" loading={this.state.loadingForm}>
          <FormErrors formErrors={this.state.formErrors} />
          <Form.Input
            placeholder="Email"
            name="email"
            value={email}
            onChange={this.handleChange}
          />

          <Form.Input
            placeholder="Password*"
            type="password"
            name="password"
            value={password}
            onChange={this.handleChange}
          />

          <div className="button-signup">
            <div className="get-started" onClick={this.submit}>
              Login
            </div>
          </div>
          <div className="term-and-conditions">
            <div>
              <small>
                Don't have an account yet,{" "}
                <Link to="/" className="direct-to-signup">
                  sign up here
                </Link>
              </small>
            </div>
            <small className="forgot-password">
            <Link to="/forgot-password">Forgot password?</Link></small>
          </div>
        </Form>
      </div>
    );
  }
}

export default SignUpForm;
