import React from "react";
import { Form } from "semantic-ui-react";
import { withRouter } from "react-router-dom";

import FormErrors from "../errorMessage";
import ModalTermAndConditions from "../termAndConditions";

class SignUpForm extends React.Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    rePassword: "",
    formErrors: {
      firstName: "",
      lastName: "",
      email: "",
      password: ""
    },
    emailValid: false,
    firstNameValid: false,
    lastNameValid: false,
    passwordValid: false,
    rePasswordValid: false,
    openModal: false,
    loadingForm: false
  };

  handleChange = (e, { name, value }) => {
    this.setState(
      {
        [name]: value
      },
      () => {
        this.validateField(name, value);
      }
    );
  };

  validateField = (fieldName, value) => {
    let fieldValidationErrors = this.state.formErrors;
    let firstNameValid = this.state.firstNameValid;
    let lastNameValid = this.state.lastNameValid;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;
    let rePasswordValid = this.state.rePasswordValid;

    switch (fieldName) {
      case "firstName":
        firstNameValid = value.match(/^[a-zA-Z ]*$/i);
        fieldValidationErrors.firstName = firstNameValid
          ? ""
          : "First name is invalid, please only use alphabetic characters";
        break;
      case "lastName":
        lastNameValid = value.match(/^[a-zA-Z ]*$/i);
        fieldValidationErrors.lastName = lastNameValid
          ? ""
          : "Last name is invalid, please only use alphabetic characters";
        break;
      case "email":
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailValid ? "" : "Email is invalid";
        break;
      case "password":
        passwordValid = value.length >= 8;
        fieldValidationErrors.password = passwordValid
          ? ""
          : "Password is too short";
        break;
      case "rePassword":
        rePasswordValid = value === this.state.password;
        fieldValidationErrors.rePassword = rePasswordValid
          ? ""
          : "Your password is not match!";
        break;
      default:
        break;
    }
    this.setState({
      formErrors: fieldValidationErrors,
      emailValid: emailValid,
      passwordValid: passwordValid,
      firstNameValid: firstNameValid,
      lastNameValid: lastNameValid,
      rePasswordValid: rePasswordValid
    });
  };

  submit = async e => {
    e.preventDefault();
    if (!this.state.firstName) {
      alert("Name is required");
    } else if (!this.state.lastName) {
      alert("Name is required");
    } else if (!this.state.email) {
      alert("Email is required");
    } else if (!this.state.password) {
      alert("Password is required");
    } else if (!this.state.rePassword) {
      alert("rePassword is required");
    } else {
      localStorage.setItem("firstName", this.state.firstName);
      localStorage.setItem("lastName", this.state.lastName);
      localStorage.setItem("email", this.state.email);

      await this.setState({
        firstName: "",
        lastName: "",
        password: "",
        email: "",
        rePassword: "",
        loadingForm: true
      });

      setTimeout(async () => {
        await this.setState({
          loadingForm: false
        });

        await alert(
          `Thanks for signing up ${localStorage.getItem(
            "firstName"
          )} ${localStorage.getItem(
            "lastName"
          )}. A confirmation email has been sent to ${localStorage.getItem(
            "email"
          )}, please check your email for further information.`
        );

        this.props.history.push("/login");
      }, 1500);
    }
  };

  closeModal = () => this.setState({ openModal: false });
  showModal = () => this.setState({ openModal: true });

  render() {
    const { firstName, lastName, email, password, rePassword } = this.state;
    return (
      <div className="form-content">
        <Form size="large" loading={this.state.loadingForm}>
          <FormErrors formErrors={this.state.formErrors} />

          <Form.Group widths="equal">
            <Form.Input
              fluid
              placeholder="First name"
              name="firstName"
              value={firstName}
              onChange={this.handleChange}
            />
            <Form.Input
              fluid
              placeholder="Last Name"
              name="lastName"
              value={lastName}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Input
            placeholder="Email"
            name="email"
            value={email}
            onChange={this.handleChange}
          />
          <Form.Field className="password-field-group" inline={true} width={16}>
            <Form.Input
              placeholder="Password*"
              type="password"
              name="password"
              value={password}
              onChange={this.handleChange}
            />
            <small className="password-explanation">
              *Please use a password at least 8 characters
            </small>
          </Form.Field>
          <Form.Input
            placeholder="Confirm Password"
            type="password"
            name="rePassword"
            value={rePassword}
            onChange={this.handleChange}
          />

          <div className="button-signup">
            <div className="get-started" onClick={this.submit}>
              get started!
            </div>
          </div>
          <div className="term-and-conditions">
            <div>
              <small>
                By signing up you agree to our{" "}
                <ModalTermAndConditions
                  openModal={this.showModal}
                  closeModal={this.closeModal}
                />
              </small>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

export default withRouter(SignUpForm);
