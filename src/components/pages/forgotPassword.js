import React from "react";
import { Input, Form, Button, Container } from "semantic-ui-react";
import InvesteurLogo from "../../images/Investeur-logo.png";

class ForgotPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      emailValid: false,
      loadingForm: false,
      emailErrorMessage: "",
    };
  }

  handleChange = (e, { name, value }) => {
    this.setState(
      {
        [name]: value
      }
    );
  };

  submit = async e => {
    e.preventDefault();
    if (!this.state.email) {
      alert("Email is required");
    } else {
      localStorage.setItem("email", this.state.email);

      await this.setState({
        email: "",
        loadingForm: true
      });

      setTimeout(async () => {
        await this.setState({
          loadingForm: false
        });

        alert(`Link to reset password has been sent to ${localStorage.getItem(
          "email"
        )}. Please check your email and follow the further instruction`);
      }, 1500);
    }
  };

  render() {
    return (
      <div className="forgot-password-page">
        <Container>
          <div className="investeur-logo">
            <img src={InvesteurLogo} alt="investeur-logo" />
          </div>
          <div className="content-fp">
            <div className="title-fp">
              <h3>Forgot Password</h3>
            </div>
            <Form loading={this.state.loadingForm}>
              <Form.Field>
                <label className="label-email">
                  <h3>Email</h3>
                </label>
                <Input size="huge" transparent name="email" onChange={this.handleChange} value={this.state.email}/>
              </Form.Field>
              <div className="button-submit-fp">
                <Button
                  animated="fade"
                  fluid
                  className="button-submit-fp"
                  onClick={this.submit}
                >
                  <Button.Content visible>Reset Password</Button.Content>
                  <Button.Content hidden>Now</Button.Content>
                </Button>
              </div>
            </Form>
          </div>
        </Container>
      </div>
    );
  }
}

export default ForgotPassword;
