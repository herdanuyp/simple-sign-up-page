import React, { Component } from "react";
import "../../App.css";
import { Grid } from "semantic-ui-react";
import { Link } from "react-router-dom";

import Facebook from "../socialMediaLink/facebook";
import Linkedin from "../socialMediaLink/linkedin";
import SignUpForm from "../form/signUpForm";

import logo from "../../images/sign_up_page_graphics.png";
import logoWithName from "../../images/logo_with_name.png";

class SignUpPage extends Component {
  render() {
    return (
      <div className="App">
        <div className="content-section">
          <Grid columns={2} stackable>
            <Grid.Column computer={16} tablet={16} mobile={16}>
              <Grid columns={2} style={{ marginTop: 10 }}>
                <Grid.Column mobile={8}>
                  <div className="the-startup-buddy-social-with">
                    <img src={logoWithName} alt="logo-the-buddy" />
                  </div>
                </Grid.Column>
                <Grid.Column mobile={8} textAlign="right">
                  <div className="login-button"><Link to ="/login">Log in</Link></div>
                </Grid.Column>
              </Grid>
            </Grid.Column>

            <Grid.Row
              className="head-fc-content"
              computer={16}
              tablet={16}
              mobile={16}
              container
            >
              <Grid.Column computer={10} tablet={8}>
                <div className="fc-content">
                  <div className="rocket-logo">
                    <img src={logo} alt="logo-rocket" />
                  </div>

                  <div className="middle-description">
                    <div className="everything-you-need">
                      everything you need
                    </div>
                    <div className="everything-you-need-description">
                      to build a startup geared toward success
                    </div>
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column computer={6} tablet={8} container>
                <div className="form-content-head ">
                  <Facebook />
                  <Linkedin />
                  <div id="or">OR</div>
                  <SignUpForm />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default SignUpPage;
