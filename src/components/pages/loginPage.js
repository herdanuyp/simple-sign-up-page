import React, { Component } from "react";
import "../../App.css";
import { Grid, Header, Form } from "semantic-ui-react";
import { withRouter } from "react-router-dom";

import Facebook from "../socialMediaLink/facebookLogin";
import Linkedin from "../socialMediaLink/linkedinLogin";
import LoginForm from "../form/loginForm";

import logoWithName from "../../images/logo_with_name.png";

class SignUpPage extends Component {
  render() {
    return (
      <Grid style={{ height: "100vh", marginTop: 10 }} textAlign="center">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header textAlign="center">
            <div className="the-startup-buddy-social-with">
              <img src={logoWithName} alt="logo-the-buddy" />
            </div>
          </Header>
          <Form size="large">
            <div className="form-content-head-login">
              <div className="member-login">Member Login</div>
              <div className="login-box">
                <Facebook />
                <Linkedin />
                <div id="or-login">OR</div>
                <LoginForm />
              </div>
            </div>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

export default withRouter(SignUpPage);
