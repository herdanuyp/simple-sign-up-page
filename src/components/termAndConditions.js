import React from "react";
import { Button, Header, Icon, Modal } from "semantic-ui-react";

class ModalTermAndConditions extends React.Component {
  state = { modalOpen: false };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    return (
      <Modal
        trigger={<small onClick={this.handleOpen} className="modal-trigger">Terms & Conditions.</small>}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        dimmer="blurring"
        size="small"
      >
        <Header icon="browser" content="Terms and Conditions" />
        <Modal.Content className="modal-content" scrolling>
          <Modal.Description>
            <p>
              Unfortunately some people don’t use the internet and websites like
              ours in the best interest of others. That is why below you find
              the Teur Pte. Ltd. / Investeur to legally protect us and yourself.
              The text doesn’t read like a good novel, but the intention is
              honest and we worked diligently to formulate it fairly and
              correctly. If we did not entirely succeed, please let us know via{" "}
              <u>info@investeur.co</u> so we can check and change in applicable
              cases.
            </p>
            <h2>TERMS AND CONDITIONS</h2>
            <ol className="ol-introduction">
              <li>
                <b>Introduction</b>
              </li>
            </ol>
            <p>
              These Terms and Conditions ("Terms", "Agreement" or “Terms and
              Conditions”) are an agreement between the operator of{" "}
              <u>www.investeur.co</u> ("Website operator", "us", "we" or "our")
              and you ("user", "you" or "your"). This Agreement sets forth the
              general Terms and Conditions of your use of the Website{" "}
              <u>www.investeur.co</u> and any of its products or services
              (collectively, "Website" or "Services").
            </p>
            <p>
              These Terms and Conditions written on this webpage shall manage
              your use of this website. These Terms will be applied fully and
              affect your use of this Website. By using this Website, you agree
              to accept all Terms and Conditions written in here. You must not
              use this Website if you disagree with any of these Terms and
              Conditions.
            </p>
            <p>
              You affirm that you are either more than 18 years of age, or
              possess legal parental or guardian consent, and are fully able and
              competent to enter into the terms, conditions, obligations,
              affirmations, representations, and warranties set forth in these
              Terms and Conditions, and to abide by and comply with these Terms
              and Conditions.
            </p>
            <p>
              Investeur is a web service built on the Sharetribe platform. The
              general terms of <u>Sharetribe </u>also apply to the users of the
              services offered via this Website.
            </p>
            <b>Intellectual Property Rights</b>
            <p>
              Other than the content you own, under these Terms, Teur Pte Ltd
              and/or its licensors own all the intellectual property rights and
              materials contained in this Website. You are granted limited
              license only for purposes of viewing and/or sharing with referring
              to this Website as the source the material.{" "}
            </p>
            <p>
              This Agreement does not transfer from Website operator to you, any
              Website operator or third party intellectual property, and all
              right, title, and interest in and to such property will remain (as
              between the parties) solely with Website operator. All trademarks,
              service marks, graphics and logos used in connection with our
              Website or Services, are trademarks or registered trademarks of
              Website operator or Website operator licensors. Other trademarks,
              service marks, graphics and logos used in connection with our
              Website or Services may be the trademarks of other third parties.
              Your use of our Website and Services grants you no right or
              license to reproduce or otherwise use any Website operator or
              third-party trademarks unless by clearly referring to this Website
              as the source.
            </p>
            <b>Accounts and membership</b>
            <p>
              If you create an account at the Website, you are responsible for
              maintaining the security of your account and you are fully
              responsible for all activities that occur under the account and
              any other actions taken in connection with it. Providing false
              contact information of any kind may result in the termination of
              your account. You must immediately notify us of any unauthorized
              uses of your account or any other breaches of security. We will
              not be liable for any acts or omissions by you, including any
              damages of any kind incurred as a result of such acts or
              omissions. We may suspend, disable, or delete your account (or any
              part thereof) if we determine that you have violated any provision
              of this Agreement or that your conduct or content would tend to
              damage our reputation and goodwill. If we delete your account for
              the foregoing reasons, you may not re-register for our Services.
              We may block your email address and Internet protocol address to
              prevent further registration.
            </p>
            <b>Removal of a user</b>
            <p>
              We reserve the right to remove any users from the Website and
              terminate their right of use of the service without any specific
              reason and without being liable for compensation.
            </p>
            <b>Website access</b>
            <p>
              You are responsible for all access to the Website using your
              internet connection, even if the access is by another person.{" "}
            </p>
            <p>
              No guarantees of the functioning of the Website service are given.
              The users are themselves responsible for their actions entering
              into services and they should estimate the reliability of other
              users before dealing with them. The Website operator can under no
              circumstances be liable for damage that is caused to the user.{" "}
            </p>
            <p>
              We make no promise that the materials on the Website are
              appropriate or available for use in locations outside Singapore.
              Accessing the Website from territories where its contents are
              illegal or unlawful is prohibited. If you choose to access the
              Website from elsewhere, you do so on your own initiative and are
              responsible for compliance with local laws and regulations.{" "}
            </p>
            <b>Restrictions</b>
            <p>You are restricted to</p>
            <ul class="small">
              <li>publish any Website material in any other media;</li>
              <li>
                sell, sublicense and/or otherwise commercialize any Website
                material;
              </li>
              <li>publicly perform and/or show any Website material;</li>
            </ul>
            <p>unless </p>
            <ul class="small">
              <li>you share the materials in support of the Website;</li>
              <li>
                your sharing does not harm the Website or its users in any way;
              </li>
              <li>
                you inform us prior to using the materials in case you are not
                certain and we provide specific approval.
              </li>
            </ul>
            <p>You are restricted to</p>
            <ul class="small">
              <li>
                use this Website in any way that is or may be damaging to this
                Website;
              </li>
              <li>
                use this Website in any way that impacts user access to this
                Website;
              </li>
              <li>
                use this Website contrary to applicable laws and regulations, or
                in any way may cause harm to the Website, or to any person or
                business entity;
              </li>
              <li>
                to engage in any data mining, data harvesting, data extracting
                or any other similar activity in relation to this Website unless
                specific approval has been granted by us in writing.
              </li>
            </ul>
            <p>You agree that you will not: </p>
            <ul class="small">
              <li>
                Use the Website for any fraudulent or unlawful purpose or
                solicit others to perform or participate in any unlawful acts;{" "}
              </li>
              <li>
                Violate any international, federal, provincial or state
                regulations, rules, laws, or local ordinances;{" "}
              </li>
              <li>
                Infringe upon or violate our intellectual property rights or the
                intellectual property rights of others;
              </li>
              <li>
                Use the Website to defame, abuse, harass, stalk, threaten or
                otherwise violate the rights of others, including without
                limitation others’ privacy rights or rights of publicity;{" "}
              </li>
              <li>
                Impersonate any person or entity, false state or otherwise
                misrepresent your affiliation with any person or entity in
                connection with the Website or express or imply that we endorse
                any statement you make;{" "}
              </li>
              <li>
                Interfere with or disrupt the operation of the Website or the
                servers or networks used to make the Website available or
                violate any requirements, procedures, policies or regulations of
                such networks.{" "}
              </li>
            </ul>
            <p>
              Certain areas of this Website may be restricted from being
              accessed by you. Teur Pte Ltd may further restrict access by you
              to any areas of this Website, at any time, in absolute discretion.
              Any user ID and password you may have for this Website are
              confidential and you must maintain confidentiality as well.
            </p>
            <b>Third party websites</b>
            <p>
              The Website does provide links to other websites and online
              resources. Although this Website may be linked to other websites,
              we are not, directly or indirectly, implying any approval,
              association, sponsorship, endorsement, or affiliation with any
              linked website, unless specifically stated herein. We are not
              responsible for examining or evaluating, and we do not warrant the
              offerings of, any businesses or individuals or the content of
              their websites. We do not assume any responsibility or liability
              for the actions, products, services and content of any other third
              parties. You should carefully review the legal statements and
              other conditions of use of any website which you access through a
              link from this Website. Your linking to any other off-site pages
              or other websites is at your own risk.
            </p>
            <p>
              You may create a link to this Website, provided that the link is
              fair and legal and is not presented in a way that is:
            </p>
            <ul class="small">
              <li>
                Misleading or could suggest any type of association, approval or
                endorsement by us that does not exist, or{" "}
              </li>
              <li>
                Harmful to our reputation or the reputation of any of our
                affiliates;
              </li>
            </ul>
            <p>
              We reserve the right to require you to immediately remove any link
              to the Website at any time and you shall immediately comply with
              any request by us to remove any such link.
            </p>
            <b>Other relevant terms and policies</b>
            <p>
              On this and all other websites run by Teur Pte. Ltd. / Investeur
              other relevant terms are:
            </p>
            <ul class="small">
              <li>
                Privacy Policy - To be found at <u>www.investeur.co/terms</u>
              </li>
              <li>
                Non disclosure agreement - To be found at{" "}
                <u>www.investeur.co/terms</u>
              </li>
              <li>
                Transaction agreement - You will be asked to read and
                acknowledge this agreement when you are buying a third party
                service through any of our websites. It clarifies that you are
                aware Teur Pte. Ltd. is not the selling party.
              </li>
            </ul>
            <b>Your Content</b>
            <p>
              In these Terms and Conditions, “Your Content” shall mean any
              audio, video text, images or other material you choose to display
              on this Website.{" "}
            </p>
            <p>
              We may, but have no obligation to, monitor Content on the Website
              submitted or created using our Services by you. By displaying Your
              Content, you grant Teur Pte Ltd a non-exclusive, worldwide
              irrevocable, sub licensable license to use, reproduce, adapt,
              publish, translate and distribute it in any and all media.
            </p>
            <p>
              Your Content must be your own and must not be invading any
              third-party’s rights. Teur Pte Ltd reserves the right to remove
              any of Your Content from this Website at any time without notice.
            </p>
            <p>
              We have the right, though not the obligation, to, in our own sole
              discretion, refuse, change or remove any Content that, in our
              reasonable opinion, violates any of our policies or is in any way
              harmful or objectionable for the Website or other our users.
            </p>
            <b>Backups</b>
            <p>
              We perform regular backups of the Website and Content and will do
              our best to ensure completeness and accuracy of these backups. In
              the event of the hardware failure or data loss we will restore
              backups automatically to minimize the impact and downtime.
            </p>
            <b>Limitation of liability</b>
            <p>
              In no event shall Teur Pte Ltd, nor any of its officers, directors
              and employees, be held liable for anything arising out of or in
              any way connected with your use of this Website whether such
              liability is under contract. Teur Pte Ltd, including its officers,
              directors and employees shall not be held liable for any indirect,
              consequential or special liability arising out of or in any way
              related to your use of this Website.
            </p>
            <b>Indemnification</b>
            <p>
              You agree to indemnify and hold Website operator and its
              affiliates, directors, officers, employees, and agents harmless
              from and against any liabilities, losses, damages or costs,
              including reasonable attorneys' fees, incurred in connection with
              or arising from any third-party allegations, claims, actions,
              disputes, or demands asserted against any of them as a result of
              or relating to your Content, your use of the Website or Services
              or any willful misconduct on your part including breach of any of
              the provisions of these Terms.
            </p>
            <b>Severability</b>
            <p>
              If any provision of these Terms is found to be invalid under any
              applicable law, such provisions shall be deleted without affecting
              the remaining provisions herein.
            </p>
            <b>Changes and amendments</b>
            <p>
              Teur Pte Ltd is permitted to revise these Terms at any time as it
              sees fit, and by using this Website you are expected to review
              these Terms on a regular basis.We reserve the right to modify this
              Agreement or its policies relating to the Website or Services at
              any time, effective upon posting of an updated version of this
              Agreement on the Website. When we do we will revise the updated
              date at the bottom of this page. Continued use of the Website
              after any such changes shall constitute your consent to such
              changes.{" "}
            </p>
            <b>Assignment</b>
            <p>
              Teur Pte Ltd is allowed to assign, transfer, and subcontract its
              rights and/or obligations under these Terms without any
              notification. However, you are not allowed to assign, transfer, or
              subcontract any of your rights and/or obligations under these
              Terms.
            </p>
            <b>Entire Agreement</b>
            <p>
              These Terms constitute the entire agreement between Teur Pte Ltd
              and you in relation to your use of this Website, and supersede all
              prior agreements and understandings.
            </p>
            <b>Acceptance of these terms</b>
            <p>
              You acknowledge that you have read this Agreement and agree to all
              its terms and conditions. By using the Website or its Services you
              agree to be bound by this Agreement. If you do not agree to abide
              by the terms of this Agreement, you are not authorized to use or
              access the Website and its Services.
            </p>
            <b>Governing Law &amp; Jurisdiction</b>
            <p>
              These Terms will be governed by and interpreted in accordance with
              the laws of the State of Singapore, and you submit to the
              non-exclusive jurisdiction of the state and federal courts located
              in Singapore for the resolution of any disputes.
            </p>
            <b>Contacting us</b>
            <p>
              If you have any questions about this Policy, please contact us
              under{" "}
              <a
                href="/cdn-cgi/l/email-protection"
                class="__cf_email__"
                data-cfemail="bfd6d1d9d0ffd6d1c9dacccbdacacd91dcd091"
              >
                [email&#160;protected]
              </a>{" "}
              This document was last updated on March 2018
            </p>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color="green" onClick={this.handleClose} inverted>
            <Icon name="checkmark" /> Got it!
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ModalTermAndConditions;
