import React from "react";
import { Image } from "semantic-ui-react";

import logoFacebook from "../../images/facebook.png";

const Facebook = () => {
  return (
    <div className="background-continue-with-facebook">
    <a href="https://en-gb.facebook.com/login/" style={{display: "flex"}}>
      <div className="icon-facebook">
        <Image src={logoFacebook} />
      </div>
      <div className="title-continue-with-facebook">Continue with Facebook</div>
      </a>
    </div>
  );
};

export default Facebook;
