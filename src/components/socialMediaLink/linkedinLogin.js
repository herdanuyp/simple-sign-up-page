import React from "react";
import { Image } from "semantic-ui-react";

import logoLinkedin from "../../images/linkedin.jpg";

const Linkedin = () => {
  return (
    <div className="background-continue-with-linkedin-login">
      <a
        href="https://www.linkedin.com/uas/login?trk=nav_header_signin"
        style={{ display: "flex" }}
      >
        <div className="icon-linkedin">
          <Image src={logoLinkedin} />
        </div>
        <div className="title-continue-with-linkedin">
          Continue with Linkedin
        </div>
      </a>
    </div>
  );
};

export default Linkedin;
